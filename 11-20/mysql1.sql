mysql> select * from stu,stu_info;
+-----+------+------+------+-----+-----+------+--------+
| sid | name | sex  | age  | cid | sid | addr | father |
+-----+------+------+------+-----+-----+------+--------+
|   1 | 小明 | 男   |   18 |   1 |   1 | 广东 | 老明   |
|   2 | 小花 | 女   |   19 |   1 |   1 | 广东 | 老明   |
|   3 | 小木 | 男   |   20 |   2 |   1 | 广东 | 老明   |
|   1 | 小明 | 男   |   18 |   1 |   2 | 河北 | 老花   |
|   2 | 小花 | 女   |   19 |   1 |   2 | 河北 | 老花   |
|   3 | 小木 | 男   |   20 |   2 |   2 | 河北 | 老花   |
|   1 | 小明 | 男   |   18 |   1 |   3 | 四川 | 老木   |
|   2 | 小花 | 女   |   19 |   1 |   3 | 四川 | 老木   |
|   3 | 小木 | 男   |   20 |   2 |   3 | 四川 | 老木   |
+-----+------+------+------+-----+-----+------+--------+
9 rows in set (0.02 sec)

mysql> select * from stu,stu_info where stu.sid = stu_info.sid;
+-----+------+------+------+-----+-----+------+--------+
| sid | name | sex  | age  | cid | sid | addr | father |
+-----+------+------+------+-----+-----+------+--------+
|   1 | 小明 | 男   |   18 |   1 |   1 | 广东 | 老明   |
|   2 | 小花 | 女   |   19 |   1 |   2 | 河北 | 老花   |
|   3 | 小木 | 男   |   20 |   2 |   3 | 四川 | 老木   |
+-----+------+------+------+-----+-----+------+--------+
3 rows in set (0.00 sec)

mysql> select * from stu,stu_info where stu.sid = stu_info.sid and stu.name="小明";
+-----+------+------+------+-----+-----+------+--------+
| sid | name | sex  | age  | cid | sid | addr | father |
+-----+------+------+------+-----+-----+------+--------+
|   1 | 小明 | 男   |   18 |   1 |   1 | 广东 | 老明   |
+-----+------+------+------+-----+-----+------+--------+
1 row in set (0.00 sec)

mysql> select * from stu,class;
+-----+------+------+------+-----+-----+-------+-------+---------+
| sid | name | sex  | age  | cid | cid | cname | cdesc | teacher |
+-----+------+------+------+-----+-----+-------+-------+---------+
|   1 | 小明 | 男   |   18 |   1 |   1 | P1701 | xxx   | 老黄    |
|   1 | 小明 | 男   |   18 |   1 |   2 | P1702 | aaa   | 老何    |
|   2 | 小花 | 女   |   19 |   1 |   1 | P1701 | xxx   | 老黄    |
|   2 | 小花 | 女   |   19 |   1 |   2 | P1702 | aaa   | 老何    |
|   3 | 小木 | 男   |   20 |   2 |   1 | P1701 | xxx   | 老黄    |
|   3 | 小木 | 男   |   20 |   2 |   2 | P1702 | aaa   | 老何    |
+-----+------+------+------+-----+-----+-------+-------+---------+
6 rows in set (0.00 sec)

mysql> select * from stu,class where stu.cid=class.cid;
+-----+------+------+------+-----+-----+-------+-------+---------+
| sid | name | sex  | age  | cid | cid | cname | cdesc | teacher |
+-----+------+------+------+-----+-----+-------+-------+---------+
|   1 | 小明 | 男   |   18 |   1 |   1 | P1701 | xxx   | 老黄    |
|   2 | 小花 | 女   |   19 |   1 |   1 | P1701 | xxx   | 老黄    |
|   3 | 小木 | 男   |   20 |   2 |   2 | P1702 | aaa   | 老何    |
+-----+------+------+------+-----+-----+-------+-------+---------+
3 rows in set (0.00 sec)

mysql> select * from stu,stu_test;
+-----+------+------+------+-----+-----+-----+-------+
| sid | name | sex  | age  | cid | sid | tid | socre |
+-----+------+------+------+-----+-----+-----+-------+
|   1 | 小明 | 男   |   18 |   1 |   1 |   1 |    70 |
|   2 | 小花 | 女   |   19 |   1 |   1 |   1 |    70 |
|   3 | 小木 | 男   |   20 |   2 |   1 |   1 |    70 |
|   1 | 小明 | 男   |   18 |   1 |   1 |   2 |    67 |
|   2 | 小花 | 女   |   19 |   1 |   1 |   2 |    67 |
|   3 | 小木 | 男   |   20 |   2 |   1 |   2 |    67 |
|   1 | 小明 | 男   |   18 |   1 |   1 |   3 |    90 |
|   2 | 小花 | 女   |   19 |   1 |   1 |   3 |    90 |
|   3 | 小木 | 男   |   20 |   2 |   1 |   3 |    90 |
|   1 | 小明 | 男   |   18 |   1 |   2 |   1 |    78 |
|   2 | 小花 | 女   |   19 |   1 |   2 |   1 |    78 |
|   3 | 小木 | 男   |   20 |   2 |   2 |   1 |    78 |
|   1 | 小明 | 男   |   18 |   1 |   2 |   2 |   100 |
|   2 | 小花 | 女   |   19 |   1 |   2 |   2 |   100 |
|   3 | 小木 | 男   |   20 |   2 |   2 |   2 |   100 |
|   1 | 小明 | 男   |   18 |   1 |   2 |   3 |    80 |
|   2 | 小花 | 女   |   19 |   1 |   2 |   3 |    80 |
|   3 | 小木 | 男   |   20 |   2 |   2 |   3 |    80 |
|   1 | 小明 | 男   |   18 |   1 |   3 |   1 |    67 |
|   2 | 小花 | 女   |   19 |   1 |   3 |   1 |    67 |
|   3 | 小木 | 男   |   20 |   2 |   3 |   1 |    67 |
|   1 | 小明 | 男   |   18 |   1 |   3 |   2 |    90 |
|   2 | 小花 | 女   |   19 |   1 |   3 |   2 |    90 |
|   3 | 小木 | 男   |   20 |   2 |   3 |   2 |    90 |
|   1 | 小明 | 男   |   18 |   1 |   3 |   3 |   100 |
|   2 | 小花 | 女   |   19 |   1 |   3 |   3 |   100 |
|   3 | 小木 | 男   |   20 |   2 |   3 |   3 |   100 |
+-----+------+------+------+-----+-----+-----+-------+
27 rows in set (0.00 sec)

mysql> select * from stu,stu_test where stu.sid=stu_test.sid;
+-----+------+------+------+-----+-----+-----+-------+
| sid | name | sex  | age  | cid | sid | tid | socre |
+-----+------+------+------+-----+-----+-----+-------+
|   1 | 小明 | 男   |   18 |   1 |   1 |   1 |    70 |
|   1 | 小明 | 男   |   18 |   1 |   1 |   2 |    67 |
|   1 | 小明 | 男   |   18 |   1 |   1 |   3 |    90 |
|   2 | 小花 | 女   |   19 |   1 |   2 |   1 |    78 |
|   2 | 小花 | 女   |   19 |   1 |   2 |   2 |   100 |
|   2 | 小花 | 女   |   19 |   1 |   2 |   3 |    80 |
|   3 | 小木 | 男   |   20 |   2 |   3 |   1 |    67 |
|   3 | 小木 | 男   |   20 |   2 |   3 |   2 |    90 |
|   3 | 小木 | 男   |   20 |   2 |   3 |   3 |   100 |
+-----+------+------+------+-----+-----+-----+-------+
9 rows in set (0.00 sec)

mysql> select * from stu,stu_test,test where stu.sid=stu_test.sid;
+-----+------+------+------+-----+-----+-----+-------+-----+-------+
| sid | name | sex  | age  | cid | sid | tid | socre | tid | tname |
+-----+------+------+------+-----+-----+-----+-------+-----+-------+
|   1 | 小明 | 男   |   18 |   1 |   1 |   1 |    70 |   1 | 英语  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   1 |    70 |   2 | 数学  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   1 |    70 |   3 | 语文  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   2 |    67 |   1 | 英语  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   2 |    67 |   2 | 数学  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   2 |    67 |   3 | 语文  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   3 |    90 |   1 | 英语  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   3 |    90 |   2 | 数学  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   3 |    90 |   3 | 语文  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   1 |    78 |   1 | 英语  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   1 |    78 |   2 | 数学  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   1 |    78 |   3 | 语文  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   2 |   100 |   1 | 英语  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   2 |   100 |   2 | 数学  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   2 |   100 |   3 | 语文  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   3 |    80 |   1 | 英语  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   3 |    80 |   2 | 数学  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   3 |    80 |   3 | 语文  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   1 |    67 |   1 | 英语  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   1 |    67 |   2 | 数学  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   1 |    67 |   3 | 语文  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   2 |    90 |   1 | 英语  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   2 |    90 |   2 | 数学  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   2 |    90 |   3 | 语文  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   3 |   100 |   1 | 英语  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   3 |   100 |   2 | 数学  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   3 |   100 |   3 | 语文  |
+-----+------+------+------+-----+-----+-----+-------+-----+-------+
27 rows in set (0.00 sec)

mysql> select * from stu,stu_test,test where stu.sid=stu_test.sid and stu_test.tid=test.tid;
+-----+------+------+------+-----+-----+-----+-------+-----+-------+
| sid | name | sex  | age  | cid | sid | tid | socre | tid | tname |
+-----+------+------+------+-----+-----+-----+-------+-----+-------+
|   1 | 小明 | 男   |   18 |   1 |   1 |   1 |    70 |   1 | 英语  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   2 |    67 |   2 | 数学  |
|   1 | 小明 | 男   |   18 |   1 |   1 |   3 |    90 |   3 | 语文  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   1 |    78 |   1 | 英语  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   2 |   100 |   2 | 数学  |
|   2 | 小花 | 女   |   19 |   1 |   2 |   3 |    80 |   3 | 语文  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   1 |    67 |   1 | 英语  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   2 |    90 |   2 | 数学  |
|   3 | 小木 | 男   |   20 |   2 |   3 |   3 |   100 |   3 | 语文  |
+-----+------+------+------+-----+-----+-----+-------+-----+-------+
9 rows in set (0.00 sec)