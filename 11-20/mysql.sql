
库

表（表头：表结构）

行

列（字段，单元格）


写操作

读操作

No query specified


注意：

1 建库（字符集）
2 建表（至少一列，列名，列类型，列长度，列修饰）

0000001
0000002
0000003
0000004
7777777

create table stu(
	id int(7) unsigned zerofill primary key AUTO_INCREMENT COMMENT "主键",
	name char(20) not null unique COMMENT "姓名",
	sex enum("男","女","保密") default "男" COMMENT "性别",
	age tinyint unsigned default 18 COMMENT "年龄",
	birthday DATE not null,
	phone char(11) not null,
	qq char(11) default "3242342342" not null,
	hobit set('看书','游戏','上网') 
);

商品id 自增
商品标题
商品描述
商品市场价
售价
库存
图片
详情
状态 上下架
置顶
创建时间
商品单位

create table goods(
	id int(7) unsigned  primary key AUTO_INCREMENT COMMENT "主键",
	title char(20) not null  COMMENT "商品标题",
	goods_desc char(200) not null  COMMENT "商品描述",
	market_price DECIMAL(7,3) default 0 COMMENT "商品市场价",
	goods_price DECIMAL(7,3) default 0 COMMENT "售价",
	stock int(5) not null default 0 COMMENT "库存",
	goods_imgs varchar(1000) not null COMMENT "图片",
	goods_details text not null COMMENT "商品详情",
	status enum('1','0') not null default '0' COMMENT "是否上加（1上架，0没上架）",
	is_top enum('1','0') not null default '0' COMMENT "是否上加（1置顶，0没置顶）",
	unit char(10) not null default "个" COMMENT "单位",
	ctime int(10) not null COMMENT "创建时间"
);

插入数据

insert into stu (name,sex,phone) values("小明55",'男','345345'),("小明66",'男','345345');

更新数据
update stu set age="30",sex="女" where name="小明";

update stu set sex="保密"  where age<19;

删除数据

delete from stu where name="小明";

CREATE TABLE `tp_flash_sale` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '活动标题',
  `goods_id` int(10) NOT NULL COMMENT '参团商品ID',
  `price` float(10,2) NOT NULL COMMENT '活动价格',
  `goods_num` int(10) DEFAULT '1' COMMENT '商品参加活动数',
  `buy_limit` int(11) NOT NULL DEFAULT '1' COMMENT '每人限购数',
  `buy_num` int(11) NOT NULL DEFAULT '0' COMMENT '已购买人数',
  `order_num` int(10) DEFAULT '0' COMMENT '已下单数',
  `description` text COMMENT '活动描述',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `is_end` tinyint(1) DEFAULT '0' COMMENT '是否已结束',
  `goods_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;



CREATE TABLE `tp_cart` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '购物车表',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `session_id` char(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'session',
  `goods_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_sn` varchar(60) NOT NULL DEFAULT '' COMMENT '商品货号',
  `goods_name` varchar(120) NOT NULL DEFAULT '' COMMENT '商品名称',
  `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '市场价',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '本店价',
  `member_goods_price` decimal(10,2) DEFAULT '0.00' COMMENT '会员折扣价',
  `goods_num` smallint(5) unsigned DEFAULT '0' COMMENT '购买数量',
  `spec_key` varchar(64) DEFAULT '' COMMENT '商品规格key 对应tp_spec_goods_price 表',
  `spec_key_name` varchar(64) DEFAULT '' COMMENT '商品规格组合名称',
  `bar_code` varchar(64) DEFAULT '' COMMENT '商品条码',
  `selected` tinyint(1) DEFAULT '1' COMMENT '购物车选中状态',
  `add_time` int(11) DEFAULT '0' COMMENT '加入购物车的时间',
  `prom_type` tinyint(1) DEFAULT '0' COMMENT '0 普通订单,1 限时抢购, 2 团购 , 3 促销优惠',
  `prom_id` int(11) DEFAULT '0' COMMENT '活动id',
  `sku` varchar(128) DEFAULT '' COMMENT 'sku',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `goods_id` (`goods_id`),
  KEY `spec_key` (`spec_key`)




  select name from stu where age between 18 and 20;
  select * from stu where age >=18 and age <=20;


  select * from stu where id=3 or id=6 or id=9 or id=12 or id=15;


 select * from stu where id in (3,6,9,12,15);




  select name,sex,phone,if(sex='男','男孩子','女孩子') from stu;



alter table stu rename stu1;


alter table stu change qq email char(20);


alter table stu modify email char(30);


alter table stu add qq char(11);


alter table stu drop qq;


alter table stu  DROP PRIMARY KEY;

ALTER TABLE stu ADD PRIMARY KEY (id);



CREATE TABLE `stu` (
  `sid` int(7) NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL COMMENT '姓名',
  `sex` enum('男','女','保密') DEFAULT '男' COMMENT '性别',
  `age` tinyint(3) unsigned DEFAULT '18' COMMENT '年龄',
  `cid` int(7) NOT NULL COMMENT '班级id',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `class` (
  `cid` int(7) NOT NULL AUTO_INCREMENT,
  `cname` char(20) NOT NULL COMMENT '姓名',
  `cdesc` char(20) NOT NULL COMMENT '班级描述',
  `teacher` char(20) NOT NULL COMMENT '老师',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `stu_info` (
  `sid` int(7) NOT NULL ,
  `addr` char(20) NOT NULL COMMENT '地址',
  `father` char(20) NOT NULL COMMENT '父亲'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


CREATE TABLE `test` (
  `tid` int(7) NOT NULL AUTO_INCREMENT,
  `tname` char(20) NOT NULL COMMENT '科目',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `stu_test` (
  `sid` int(7) NOT NULL ,
  `tid` int(7) NOT NULL ,
  `socre` int(7) NOT NULL 
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;





select * from stu t1 inner join stu_info t2 on t1.sid=t2.sid where t1.name="小明";


select * from stu t1 inner join class t2 on t1.cid=t2.cid;


select * from stu t1 inner join stu_test t2 on t1.sid=t2.sid inner join test t3 on t2.tid=t3.tid;


select avg(t1.socre) from stu_test t1 inner join test t2 on t1.tid=t2.tid where t2.tname="英语";