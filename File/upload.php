<?php

include './Upload.class.php';

// $p = $_GET['p'];

$config  = array(
		'dirname'=>'./xbs/'
	);

$obj = new Upload($config);

$res = $obj->upload();

if($res){
	echo 'true';
}else{
	echo $obj->getError();
}

exit;

$data = current($_FILES);

// 文件没有上传到
if(empty($_FILES)){
	exit("files  empty");
}

foreach ($data['name'] as $k => $v) {
	//上传有错误
	if($data['error'][$k]){
		exit("upload  erro,error_num:".$data['error'][$k]);
	}

	//上传类型的限制
	$type = ['image/png','image/jpeg','image/gif'];

	if(!in_array($data['type'][$k], $type)){
		exit("type error,allow type:".implode(',', $type));
	}


	//上传大小限制
	if($data['size'][$k]>200*1024){
		exit("size error,allow size:100k");
	}

	//移动文件到目标地址

	$name = uniqid();

	$suffux = explode('.', $data['name'][$k]);

	$suffux = end($suffux);

	$path = $name.'.'.$suffux;

	move_uploaded_file($data['tmp_name'][$k], './Upload/'.$path);
}

header("Location:./index.php");













 ?>