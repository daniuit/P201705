<?php use yii\bootstrap\ActiveForm; 
use yii\captcha\Captcha;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php $form = ActiveForm::begin(['action' => ['test/getpost'],'method'=>'post',]); ?>
	login模板 <?php echo $name ?>

	<?= $form->field($model, 'verifyCode', [
	        'options' => ['class' => 'form-group form-group-lg'],
	])->widget(Captcha::className(),[
	       'template' => "{image}",
	       'imageOptions' => ['alt' => '验证码'],
	]); ?>
</body>
</html>