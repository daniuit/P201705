<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class LoginController extends Controller
{
    public $layout = false;
    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // var_dump($_GET['id']);

        $id = Yii::$app->request->get('id');

        // var_dump($id);
        // echo "Login";
        $posts = Yii::$app->db->createCommand('SELECT * FROM user')
            ->queryAll();

        // var_dump($posts);

        $rows = (new \yii\db\Query())->select("*")->from('question')->where(['good' => '2'])->all();

        // var_dump($rows);
        // 
        //

        $model =  new LoginForm();


        return $this->render('login', [
            'name' => "学并思",'model'=>$model
        ]);
    }

    public function actionLogin()
    {
        echo "dfdf";
    }

    public function actionShow()
    {
        echo "show";
    }

    
}
