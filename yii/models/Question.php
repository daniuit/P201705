<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property integer $qid
 * @property string $title
 * @property integer $cid
 * @property integer $uid
 * @property string $content
 * @property string $exct_info
 * @property integer $kiss
 * @property integer $answer_num
 * @property integer $view_num
 * @property string $status
 * @property string $is_top
 * @property string $is_jing
 * @property integer $ctime
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'ctime'], 'required'],
            [['cid', 'uid', 'kiss', 'answer_num', 'view_num', 'ctime'], 'integer'],
            [['content', 'status', 'is_top', 'is_jing'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['exct_info'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'qid' => 'Qid',
            'title' => '标题',
            'cid' => '分类id',
            'uid' => '用户id',
            'content' => 'Content',
            'exct_info' => 'Exct Info',
            'kiss' => 'Kiss',
            'answer_num' => 'Answer Num',
            'view_num' => 'View Num',
            'status' => 'Status',
            'is_top' => 'Is Top',
            'is_jing' => 'Is Jing',
            'ctime' => 'Ctime',
        ];
    }
}
