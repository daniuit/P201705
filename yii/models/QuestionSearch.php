<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Question;

/**
 * QuestionSearch represents the model behind the search form about `app\models\Question`.
 */
class QuestionSearch extends Question
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qid', 'cid', 'uid', 'kiss', 'answer_num', 'view_num', 'ctime'], 'integer'],
            [['title', 'content', 'exct_info', 'status', 'is_top', 'is_jing'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Question::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qid' => $this->qid,
            'cid' => $this->cid,
            'uid' => $this->uid,
            'kiss' => $this->kiss,
            'answer_num' => $this->answer_num,
            'view_num' => $this->view_num,
            'ctime' => $this->ctime,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'exct_info', $this->exct_info])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'is_top', $this->is_top])
            ->andFilterWhere(['like', 'is_jing', $this->is_jing]);

        return $dataProvider;
    }
}
