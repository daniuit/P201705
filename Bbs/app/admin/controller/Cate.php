<?php
namespace app\admin\controller;
use think\Controller;

class Cate extends Common
{
    public function index()
    {
        $cate = db('cate')->select();

        $cates = $this->getCate($cate);

        return $this->fetch('',['cates'=>$cates]);
    }

    public function add()
    {
        $data = input('');

        $res = db('cate')->insert($data);

        if($res){
            exit(json_encode(['error'=>0,'info'=>"增加成功"]));
        }else{
            exit(json_encode(['error'=>1,'info'=>"增加失败"]));
        }
    }

    public function getCate($cate,$fid=0,$cn=0)
    {
       $tempArr = [];

       foreach ($cate as $k => $row) {
           if($row['fid']==$fid){
                $row['name'] = "├".str_repeat('----', $cn).$row['name'];
                $row['level'] = $cn."级";
                
                $tempArr[] = $row;
                $data = $this->getCate($cate,$row['cid'],$cn+1);
                $tempArr = array_merge($tempArr,$data);
           }
       }
       return $tempArr;
    }

    

    
}
