<?php
namespace app\admin\controller;
use think\Controller;

class Count extends Common
{

    public function run()
    {
        
        $data['reg_num'] = 1000;

        $data['ques_num'] = 565;

        $data['re_num'] = 45454;

        $data['dateline'] = date('Y-m-d');

        db('report')->insert($data);

    }
    public function user()
    {

        $data = db('report')->order('dateline desc')->limit(7)->select();


        $dataDate = '';

        $dataNum = '';

        foreach ( $data as $key => $row) {
            
            $dataDate .= "'".$row['dateline']."',";
            $dataNum .= $row['reg_num'].",";
        }

        $dataDate = rtrim($dataDate,',');

        $dataNum = rtrim($dataNum,',');
    	

        return $this->fetch('',['dataDate'=>$dataDate,'dataNum'=>$dataNum]);
    }

    public function userajax()
    {
        $data = db('report')->order('dateline desc')->limit(7)->select();

        exit(json_encode(['error'=>0,'data'=>$data]));
    }

    
}
