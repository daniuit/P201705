<?php
namespace app\index\controller;
use think\Controller;
use weixin\Weixin;

class Jie extends Common
{

	public function index()
	{
        // var_dump($_SERVER);
        // $id = $_GET['id'];

        // var_dump($id);

        // $sql = "select * from question where qid='{$id}'";
        // 
        // $qid = input('get.id');

        // $qid = addslashes($qid);

        // $sql = "select * from question where qid='{$qid}'";

        // var_dump($sql);

        // // $res = db('question')->where('qid',$qid)->find();

        // var_dump( db('question'));

        // var_dump($res);


        // // $res = db()->query($sql);

        // // var_dump($sql);

        // // var_dump($res);

        // exit;





		$qid = input('qid');

		$ques = db('question t1')->join('user t2','t1.uid=t2.uid')->join('cate t3','t1.cid=t3.cid')->where('qid',$qid)->field('t1.*,t2.nickname,t2.face,t3.name cname')->find();

		$ques['is_collect'] = db('collect')->where(['qid'=>$qid,'uid'=>session('uid')])->find();


		$answer = db('answer t1')->join('user t2','t1.uid=t2.uid')->field('t1.*,t2.nickname,t2.face,t2.role')->where('t1.qid',$qid)->order('t1.ctime desc')->select();


		foreach ($answer as $k => $row) {
			$answer[$k]['is_zan'] = db('zan')->where(['aid'=>$row['aid'],'uid'=>session('uid')])->find();
		}


		return $this->fetch('',['ques'=>$ques,'answer'=>$answer,'title'=>$ques['title']]);
	}
    public function add()
    {

    	$question = model('vercode')->getRandOne();
    	var_dump($_SERVER['PHP_SELF']);
    	$cates = db('cate')->select();

        return $this->fetch('',['cates'=>$cates,'question'=>$question,'title'=>'增加问题']);
    }

    public function addjie()
    {

        // $data = $_POST;

        // var_dump($data);exit;

    	$data = input('post.',null,'htmlspecialchars');

        // var_dump($data);exit;

    	model('vercode')->checkcode($data['vercode']);

    	model('kiss')->checkkiss(session('uid'),$data['kiss']);

    	$validate = validate('jie');

    	if(!$validate->check($data)){
		    exit(json_encode(['error'=>1,'info'=>$validate->getError()]));
		}

		unset($data['file']);
		unset($data['vercode']);

		if($data['cid']==1){
			$data['exct_info'] = json_encode($data['exct_info']);
		}else{
			unset($data['exct_info']);
		}

		$data['uid'] = session('uid');
		$data['ctime'] = time();

		$res = db('question')->insert($data);

		$qid = db('question')->getLastInsID();

		if($res){

			db('user')->where('uid', session('uid'))->setDec('kiss',$data['kiss']);

			exit(json_encode(['error'=>0,'info'=>"发布成功",'url'=>url('index/jie/index',['qid'=>$qid])]));
		}else{
			exit(json_encode(['error'=>1,'info'=>"发布失败"]));
		}
    }

    public function upload()
    {

    	// 获取表单上传文件 例如上传了001.jpg
	    $file = request()->file('file');

	    // 移动到框架应用根目录/public/uploads/ 目录下
	    if($file){

	        $info = $file->validate(['size'=>1567800,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');

	        if($info){

	        	$face = getRootPath().'/uploads/'.date('Ymd').'/'. $info->getFilename();

	        	exit(json_encode(['code'=>0,'msg'=>"上传成功",'data'=>['src'=>$face]]));

	        }else{
	            // 上传失败获取错误信息
	            exit(json_encode(['code'=>1,'msg'=>$file->getError()]));
	        }
	    }

    }

    public function answer()
    {
    	$data = input('post.');

    	unset($data['file']);

    	$data['uid'] = session('uid');


    	$data['ctime'] = time();

    	$res = db('answer')->insert($data);

    	if($res){

    		db('question')->where('qid', $data['qid'])->setInc('answer_num');

    		$answerNum = db('answer')->where(['uid'=>$data['uid'],'ctime'=>['>',strtotime('-7 days')]])->count('aid');

    		db('user')->update(['answer_num'=>$answerNum,'uid'=>$data['uid']]);

			exit(json_encode(['error'=>0,'info'=>"回复成功"]));
		}else{
			exit(json_encode(['error'=>1,'info'=>"回复失败"]));
		}


    }

    public function collect()
    {
    	$data['qid'] = input('qid');

    	$data['uid'] = session('uid');

    	$res = db('collect')->where($data)->find();

    	if($res){

    		$res = db('collect')->where($data)->delete();

    		if($res){
    			exit(json_encode(['error'=>0,'info'=>"取消收藏"]));
    		}else{
    			exit(json_encode(['error'=>1,'info'=>"取消收藏失败"]));
    		}
    	}else{

    		$data['ctime'] = time();

    		$res = db('collect')->insert($data);

    		if($res){
    			exit(json_encode(['error'=>0,'info'=>"收藏"]));
    		}else{
    			exit(json_encode(['error'=>1,'info'=>"收藏失败"]));
    		}

    	}
    }

    public function zan()
    {
    	$data['aid'] = input('aid');

    	$data['uid'] = session('uid');

    	$res = db('zan')->where($data)->find();

    	if($res){

    		$res = db('zan')->where($data)->delete();

    		db('answer')->where('aid', $data['aid'])->setDec('zan_num');

    		if($res){
    			exit(json_encode(['error'=>0,'info'=>"取消点赞"]));
    		}else{
    			exit(json_encode(['error'=>1,'info'=>"取消点赞失败"]));
    		}
    	}else{

    		$data['ctime'] = time();

    		$res = db('zan')->insert($data);

    		db('answer')->where('aid', $data['aid'])->setInc('zan_num');

    		if($res){
    			exit(json_encode(['error'=>0,'info'=>"点赞"]));
    		}else{
    			exit(json_encode(['error'=>1,'info'=>"点赞失败"]));
    		}

    	}
    }
}