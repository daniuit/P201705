<?php
namespace app\index\controller;
use think\Controller;
use weixin\Weixin;

class Index extends Common
{
    public function test()
    {
        //路径重写、、index.php 去掉
        header('location:http://www.baidu.com');exit;
    }
    public function index()
    {

        if(!cache('cate')){

            echo 888;

            $cates = db('cate')->select();

            cache('cate',$cates,600);

        }else{

            echo 1000;

            $cates = cache('cate');
        }


        $where = input('');

        if(isset($where['cid'])){
            $where['t1.cid'] =$where['cid'];
            unset($where['cid']);
        }

        unset($where['page']);

        if(!$where){
            $top = db('question t1')->join('user t2','t1.uid=t2.uid')->join('cate t3','t1.cid=t3.cid')->field('t1.*,t2.nickname,t2.face,t3.name cname')->where('is_top','1')->limit(5)->select();

            $where['is_top']= '0';

            $ques = db('question t1')->join('user t2','t1.uid=t2.uid')->join('cate t3','t1.cid=t3.cid')->field('t1.*,t2.nickname,t2.face,t3.name cname')->where($where)->paginate(10);

            
        }else{
            $top = [];

            $ques = db('question t1')->join('user t2','t1.uid=t2.uid')->join('cate t3','t1.cid=t3.cid')->field('t1.*,t2.nickname,t2.face,t3.name cname')->where($where)->order('is_top desc')->paginate(10);
        }

        return $this->fetch('',['ques'=>$ques,'top'=>$top,'cates'=>$cates,'title'=>'一个从来没有过的社区']);
    }

    public function show()
    {
    	// cache('cate', NULL);

     //    where uid 
    }
}
