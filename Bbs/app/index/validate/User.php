<?php 
namespace app\index\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'nickname'  =>  'require|length:6,8',
        'email' =>  'require|email|unique:user',
        'repassword'=>'require|confirm:password'
    ];

    protected $message = [
	    'nickname.require' => '名称必须',
	    'nickname.length'  => '名称最多不能超过6-98个字符',
	    'email.emarequireil'  => '邮箱必须',
	    'email.email'  => '邮箱格式错误',
	    'email.unique' => '邮箱已存在',
	    'repassword'   => '两次密码不一致',
	];

	protected $scene = [
        'edit'  =>  ['nickname','email'],
        'editpass'=>['repassword'],
    ];
}
 ?>