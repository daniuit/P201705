<?php
namespace app\index\controller;
use think\Controller;

class Api extends Controller
{
    
    public function index()
    {
        $access_token = $this->get_access_token();
        var_dump($access_token);
    }

    public function get_access_token()
    {

        if(cache("access_token")){

            return cache("access_token");
        }else{
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxbac82045a637c02e&secret=7c320fea54e77a01fcc59ff7060b04e2";

            $data = file_get_contents($url);

            $data = json_decode($data,true);

            cache("access_token",$data['access_token'],7200);

            return $data['access_token'];
        }
    }


    public function setMenu()
    {
        $access_token = $this->get_access_token();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$access_token;

        $arr = ['button'=>[
        [
            'type'=>'view',
            'name'=>'百度',
            'url'=>'http://www.baidu.com'
        ],
        [
            'type'=>'view',
            'name'=>'网易',
            'url'=>'http://www.163.com'
        ],
        [
            'name'=>'有子菜单',
            'sub_button'=>[
                [
                    'type'=>'view',
                    'name'=>'网易',
                    'url'=>'http://www.163.com'
                ],
                [
                    'type'=>'view',
                    'name'=>'百度',
                    'url'=>'http://www.baidu.com'
                ]
            ]
        ],
        ]];

        // var_dump(json_encode($arr));exit;

        $this->wpost($url,$arr);

        
    }

    public function wpost($url,$data)
    {
        $data  =  json_encode($data,JSON_UNESCAPED_UNICODE);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 

        $output = curl_exec($ch);

        var_dump($output);


    }

    public function getOpenId()
    {

        $access_token = $this->get_access_token();
        $url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=".$access_token;

        $data = file_get_contents($url);

        $data = json_decode($data,true);


        return $data['data']['openid'];
    }

    public function qunfa()
    {
        
        $openids = $this->getOpenId();

        $access_token = $this->get_access_token();

        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=".$access_token;

        $arr = ['touser'=>$openids,'msgtype'=>'text','text'=>['content'=>'你好，我测试群发']];

        $this->wpost($url,$arr);
    }

    public function moban()
    {
        $access_token = $this->get_access_token();

        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$access_token;

        $arr = [
            'touser'=>'oF5Z2v5yzYfsCYP2oEDfz4ns94p0',
            'template_id'=>'CQQkgL16s7n7PezCDvR5ktaLjQ9KWG_digc_KhYA8xI',
            'url'=>'http://www.baidu.com',
            'data'=>[
                'addr'=>[
                    'value'=>'广州丽人新天地4',
                    'color'=>"#173177"
                ],
                'time'=>[
                    'value'=>date('Y-m-d H:i:s'),
                    'color'=>"#173177"
                ],
                'money'=>[
                    'value'=>'789.90',
                    'color'=>"#173177"
                ],
            ]
        ];

        $this->wpost($url,$arr);
        
    }

    
   
}
