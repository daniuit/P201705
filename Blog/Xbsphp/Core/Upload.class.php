<?php 	
/**
* 上传类
*/
class Upload
{	
	// 配置
	public $config = array(
		'dirname'=>'./Upload/',
		'maxsize'=>200000,
		'type'=>array('image/jpg','image/png','image/jpeg')
		);

	//上传数据
	public $data;


	public $error;

	//构造函数，初始化
	public function __construct($config=array())
	{
		$this->config = array_merge($this->config,$config);
	}
	//上传方法
	public function upload()
	{
		// 文件没有上传到
		if(empty($_FILES)){
			$this->error= "files  empty";
			return false;
		}

		$this->data = current($_FILES);

		if(!$this->checkError()) return false;

		if(!$this->checkType()) return false;

		if(!$this->checkSize()) return false;

		if(!$this->moveFiles()) return false;
	}

	public function getError()
	{
		return $this->error;
	}

	public function checkError()
	{
		foreach ($this->data['error'] as $k=> $v) {
			if($v>0){
				$this->error = "upload  erro,error_num:".$v."filename".$this->data['name'][$k];
				return false;
			}
		}
		return true;
	}
	public function checkType()
	{
		foreach ($this->data['type'] as $k => $v) {
			if(!in_array($v, $this->config['type'])){
				$this->error = "type error,allow type:".implode(',', $this->config['type']);
				return false;
			}
		}
		return true;
	}
	public function checkSize()
	{
		foreach ($this->data['size'] as $k => $v) {
			if($v>$this->config['maxsize']){
				$this->error = "size error,allow size:".$this->config['maxsize'];
				return false;
			}
		}
		return true;
	}
	public function moveFiles()
	{

		is_dir($this->config['dirname']) || mkdir($this->config['dirname'],0777,true);

		foreach ($this->data['tmp_name'] as $k => $v) {

			$name = uniqid();

			$suffux = explode('.', $this->data['name'][$k]);

			$suffux = end($suffux);

			$path = $name.'.'.$suffux;

			move_uploaded_file($this->data['tmp_name'][$k], $this->config['dirname'].$path);
			
		}
		return true;
	}
}












 ?>