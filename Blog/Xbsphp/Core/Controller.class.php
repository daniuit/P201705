<?php 
/**
* 控制器基类
*/
class Controller
{
	public $data=array();

	public function display($actionname='')
	{
		extract($this->data);

		if(empty($actionname)){
			include VIEW_PATH.'/'.CONTROLLER_NAME.'/'.ACTION_NAME.'.html';
		}else{
			include VIEW_PATH.'/'.CONTROLLER_NAME.'/'.$actionname.'.html';
		}
	}

	public function assign($key,$data)
	{
		$this->data[$key] = $data;
	}
}
















 ?>