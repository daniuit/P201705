<?php 

// 公共目录
define("COMMON_PATH", "./Xbsphp/Common");

define("CORE_PATH", "./Xbsphp/Core");

define("LIB_PATH", "./Xbsphp/Lib");

define("VENDOR_PATH", "./Xbsphp/Vendor");

define("__PUBLIC__", './Public');

include COMMON_PATH.'/functions.php';


function __autoload($classname)
{
	$paths = array(
		CONTROLLER_PATH.'/'.$classname.'.class.php',
		MODEL_PATH.'/'.$classname.'.class.php',
		CORE_PATH.'/'.$classname.'.class.php',
		);
	foreach ($paths as  $path) {
		if(file_exists($path)){
			require_once $path;
		}
	}
}

// var_dump($_SERVER['PATH_INFO']);exit;

$moduel = isset($_GET['m'])? ucfirst(strtolower($_GET['m'])) : config('DETALUT_MODUEL');
$controller = isset($_GET['c'])? ucfirst(strtolower($_GET['c'])) : config('DETALUT_CONTROLLER');
$action = isset($_GET['a'])? ucfirst(strtolower($_GET['a'])) : config('DETALUT_ACTION');

unset($_GET['m']);

unset($_GET['c']);

unset($_GET['a']);


define("MODULE_PATH", APP_PATH.'/'.$moduel);


define("CONTROLLER_PATH", MODULE_PATH.'/Controller');

define("VIEW_PATH", MODULE_PATH.'/View');

defined("MODEL_PATH") || define("MODEL_PATH", MODULE_PATH.'/Model');


define("MODULE_NAME", $moduel);

define("CONTROLLER_NAME", $controller);

define("ACTION_NAME", $action);

// var_dump(CONTROLLER_PATH,VIEW_PATH,MODEL_PATH);


// $arr =get_defined_constants(true);


// var_dump($arr['user']);

$controller = $controller."Controller";

$obj = new $controller();

$obj->$action();


 ?>