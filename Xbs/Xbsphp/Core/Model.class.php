<?php 

/**
* 数据库操作类
*/
class Model{
	
	public $table;//存放表
	public static $db;//存pdo链接对象
	public $error;//存错误信息
	public $where='';//存条件
	public $fileds = "*";//存字段
	public $limit='';//存limit
	/**
	 * [__construct 构造函数]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:42:39+0800
	 * @param    string                   $table [表名]
	 */
	public function __construct($table='')
	{
		$this->table = $table;

		try{
			if(!self::$db){
				self::$db = new PDO(config('DB_TYPE').":host=".config('DB_HOST').";dbname=".config('DB_NAME').";charset=utf8",config('DB_USER'),config('DB_PASSWORD'));
			}
		}catch(PDOException  $e ){
			$this->error = $e;
		}
	}
	/**
	 * [getError 获取错误信息]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:42:59+0800
	 * @return   [type]                   [description]
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * [del 删除]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:43:14+0800
	 * @param    string                   $id [主键值]
	 * @return   [type]                       [description]
	 */
	public function del($id='')
	{
		if(empty($this->table)){
			$this->error = "没有表名";
			return false;
		}


		if($id){

			$key = $this->getPrimaryKey();

			$sql = "delete from ".$this->table." where ".$key."=".$id;

		}else{

			$sql = "delete from ".$this->table.$this->where;

		}

		return $this->exec($sql);
	}
	/**
	 * [getPrimaryKey 获取主键]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:43:36+0800
	 * @return   [type]                   [description]
	 */
	public function getPrimaryKey()
	{
		$sql = "desc ".$this->table;
		$data = $this->query($sql);

		if($data){

			foreach ($data as  $row) {
				if($row['Key']=='PRI'){
					return $row['Field'];
				}
			}

		}
	}
	/**
	 * [query 查询]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:43:50+0800
	 * @param    [type]                   $sql [执行语句]
	 * @return   [type]                        [结果]
	 */
	public function query($sql)
	{
		$res = self::$db->query($sql);

		if($res){
			return $res->fetchAll(PDO::FETCH_ASSOC);
		}else{

			$this->error = self::$db->errorInfo();

			return false;
		}

		
	}
	/**
	 * [add 增加]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:44:15+0800
	 * @param    [arr]                   $data [数据]
	 */
	public function add($data)
	{
		if(empty($this->table)){
			$this->error = "没有表名";
			return false;
		}

		$keys = implode(',', array_keys($data));

		$values = implode("','", $data);

		$sql = "insert into ".$this->table." (".$keys.") values('".$values ."')";

		return $this->exec($sql);
	}
	/**
	 * [update 更新]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:44:34+0800
	 * @param    [数组]                   $data [数据]
	 * @return   [type]                         [description]
	 */
	public function update($data)
	{

		if(empty($this->table)){
			$this->error = "没有表名";
			return false;
		}

		$str= '';

		foreach ($data as $k => $v) {
			$str .= $k.'="'.$v.'",';
		}

		$str = rtrim($str,',');

		$sql = "update ".$this->table.' set '.$str.$this->where;

		return $this->exec($sql);
	}
	/**
	 * [exec pdo增删改]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:44:58+0800
	 * @param    [type]                   $sql [description]
	 * @return   [type]                        [description]
	 */
	public function exec($sql)
	{
		$res = self::$db->exec($sql);

		if($res){
			return true;
		}else{

			$this->error = self::$db->errorInfo();

			return false;
		}
	}

	/**
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:45:29+0800
	 * @param    [type]                   $where [description]
	 * @return   [type]                          [description]
	 */
	public function where($where)
	{
		$this->where = " where ".$where;
		return $this;
	}
	/**
	 * [select 查询]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:45:39+0800
	 * @param    string                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function select($id='')
	{
		if(empty($this->table)){
			$this->error = "没有表名";
			return false;
		}

		if($id){
			$key = $this->getPrimaryKey();
			$sql = "select ".$this->fileds." from ".$this->table." where " .$key ."=".$id;
		}else{

			$key = $this->getPrimaryKey();
			$sql = "select ".$this->fileds." from ".$this->table.$this->where.$this->limit;

		}
		return $this->query($sql);
	}
	/**
	 * [limit limit]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:45:48+0800
	 * @param    [type]                   $limit [description]
	 * @param    string                   $num   [description]
	 * @return   [type]                          [description]
	 */
	public function limit($limit,$num='')
	{
		if(!$num){
			$this->limit = " limit ".$limit;
		}else{
			$this->limit = " limit ".$limit.','.$num;
		}

		return $this;
	}
	/**
	 * [fileds 字段筛选]
	 * @Author   Xuebingsi
	 * @DateTime 2017-11-24T09:45:56+0800
	 * @param    [type]                   $fileds [description]
	 * @return   [type]                           [description]
	 */
	public function fileds($fileds)
	{
		$this->fileds = $fileds;

		return $this;
	}
}











 ?>