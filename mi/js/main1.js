$(function () {

	var num = 0;

	function run () {

		$('.img_item').eq(num).fadeIn().siblings().fadeOut();

		$('.click_item').eq(num).css('background', '#fff').siblings().css('background', 'red');

		if(num==$('.img_item').length-1){
			num=0
		}else{
			num++;
		}
	}

	time = setInterval(run, 1000);


	$('#banner').hover(function() {
		clearInterval(time);
	}, function() {
		time = setInterval(run, 1000);
	});


	$('.click_right').click(function(event) {
		run();
	});

	$('.click_left').click(function(event) {
		if(num==1){
			num = $('.img_item').length-1;
		}else if(num==0){
			num = $('.img_item').length-2;
		}else{
			num-=2;
		}

		run();
	});

	$('.click_item').click(function(event) {
		 index = $(this).index();
		 num = index;
		 run();
	});




	//购物车
	$('.car').hover(function() {
		$('.car .show').stop().slideToggle();
	}, function() {
		$('.car .show').stop().slideToggle();
	});



	$('.menu .menu_list a').hover(function() {
		index = $(this).index();
		$('.bg').stop().slideDown();
		$('.show_item').eq(index).show().siblings().hide();
	}, function() {
		$('.bg').stop().slideUp(900);
	});

	$('.bg').hover(function() {
		$(this).stop().slideDown();
	}, function() {
		$(this).stop().slideUp(900);
	});

})