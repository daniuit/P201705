-- 分类表

CREATE TABLE `cate` (
  `cid` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL unique  COMMENT '分类名',
  `fid` int(2) NOT NULL default 0 COMMENT '父id',
  `sort` int(2) NOT NULL   COMMENT '排序',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "分类表";

-- 问题表

CREATE TABLE `question` (
  `qid` int(10) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL   COMMENT '标题',
  `cid` int(2) NOT NULL default 0 COMMENT '分类id',
  `uid` int(7) NOT NULL default 0 COMMENT '用户id',
  `content` text NOT NULL   COMMENT '内容',
  `kiss` int(7) NOT NULL  DEFAULT 0 COMMENT '飞吻',
  `answer_num` int(7) NOT NULL  DEFAULT 0 COMMENT '回答量',
  `view_num` int(7) NOT NULL  DEFAULT 0 COMMENT '浏览量',
  `status` enum("0","1") default "0" COMMENT "0未结，1已结",
  `is_top` enum("0","1") default "0" COMMENT "0普通，1置顶",
  `is_jing` enum("0","1") default "0" COMMENT "0普通，1精华",
  `ctime` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`qid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "问题表";



-- 用户表

CREATE TABLE `user` (
  `uid` bigint(10) NOT NULL AUTO_INCREMENT,
  `email` char(30) NOT NULL unique  COMMENT '邮箱',
  `nickname` char(30) NOT NULL unique  COMMENT '昵称',
  `password` char(32) NOT NULL  COMMENT '密码',
  `sex` enum("男","女") default "男" COMMENT "性别",
  `role` char(32) NOT NULL  COMMENT '角色',
  `city` char(20) NOT NULL  COMMENT '城市',
  `sign` char(50) NOT NULL  COMMENT '签名',
  `face` char(100) NOT NULL  COMMENT '头像',
  `openid` char(100) NOT NULL  COMMENT '第三方openid',
  `kiss` int(7) NOT NULL  DEFAULT 100 COMMENT '飞吻',
  `ctime` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "用户表";

-- 回答表

CREATE TABLE `answer` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `qid` int(2) NOT NULL default 0 COMMENT '问题id',
  `uid` int(7) NOT NULL default 0 COMMENT '用户id',
  `content` text NOT NULL   COMMENT '内容',
  `status` enum("0","1") default "0" COMMENT "0没被采纳，1采纳",
  `ctime` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "回答表";

-- 点赞表
CREATE TABLE `zan` (
  `aid` int(2) NOT NULL default 0 COMMENT '回答id',
  `uid` int(7) NOT NULL default 0 COMMENT '用户id',
  `ctime` int(10) NOT NULL COMMENT '创建时间',
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT "点赞表";


-- 收藏表

CREATE TABLE `collect` (
  `qid` int(2) NOT NULL default 0 COMMENT '问题id',
  `uid` int(7) NOT NULL default 0 COMMENT '用户id',
  `ctime` int(10) NOT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT "收藏表";

-- 友情链接表
CREATE TABLE `link` (
  `lid` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL  COMMENT '链接名',
  `url` char(50) NOT NULL  COMMENT '链接url',
  `sort` int(2) NOT NULL   COMMENT '排序',
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "友情链接表";

-- 验证表
CREATE TABLE `vercode` (
  `vid` int(10) NOT NULL AUTO_INCREMENT,
  `question` char(30) NOT NULL   COMMENT '问题',
  `answer` char(30) NOT NULL   COMMENT '答案',
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "验证表";

-- 消息表
CREATE TABLE `msg` (
  `mid` int(10) NOT NULL AUTO_INCREMENT,
  `suid` int(7) NOT NULL default 0 COMMENT '源uid',
  `duid` int(7) NOT NULL default 0 COMMENT '目标uid',
  `status` enum("0","1",'2','3') COMMENT "0回答，1点赞，2收藏，3@你",
  `qid` int(2) NOT NULL default 0 COMMENT '问题id',
  `ctime` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "消息表";
-- 签到表

CREATE TABLE `signin` (
  `sid` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(7) NOT NULL default 0 COMMENT '用户id',
  `dateline` date NOT NULL   COMMENT '日期',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT "签到表";
