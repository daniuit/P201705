<?php


$a  = true;


// if(条件){
// 	成立执行的代码块
// }

// if($a){
// 	if(3>5){
// 		$a = 1000;
// 	}else{
// 		$a = 2000;
// 	}
	
// }elseif(1==1){
// 	$a = 9000;
// }else{
// 	$a = 8000;
// } 

// $b= 10;

// switch ($b) {
// 	case '10':
// 		echo "a888";
// 		break;
// 	case 'b':
// 		echo "b888";
// 		break;
// 	case 'c':
// 		echo "c888";
// 		break;
// 	default:
// 		echo "default";
// 		break;
// }


// for (前置动作; 条件 ; 后置动作) { 
// 	循环体	
// }

echo "<table border='1'>";

for ($i=0; $i < 10; $i++) { 
	echo "<tr>";

	for ($j=0; $j <10 ; $j++) { 
		echo "<td>";
		echo $j;
		echo "</td>";
	}
	echo "</tr>";
}
echo "</table>";
 ?>
 <!doctype html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 </head>
 <body>
 	<table border="1">
 		
 		<?php for ($i=0; $i <10 ; $i++) { ?>
			
			<tr>
				
				<?php for ($j=0; $j <10 ; $j++) { ?> 
					<td>
						<?php echo $i.'*'.$j.'='.$i*$j ?>
					</td>
				<?php } ?>
			</tr>
 			
 		<?php } ?>
 	</table>
 </body>
 </html>
