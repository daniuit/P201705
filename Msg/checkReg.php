<?php

$name = $_POST['name'];

switch ($name) {
	case 'email':
		checkEmail();
		break;
	case 'phone':
		checkPhone();
		break;
	case 'username':
		checkUsername();
		break;
	case 'password':
		checkPassword();
		break;
	default:
		
		break;
}

 function checkEmail()
{
	$email = $_POST['data'];

	$preg = "/^([\da-z]+[\da-z_]*[\da-z]+|[\da-z])@([\da-z]+[\da-z-]*[\da-z]+|[\da-z])(\.[a-z]{2,})+$/";

	$c = preg_match($preg, $email);

	if(!$c){
		echo json_encode(['error'=>1,'info'=>'邮箱格式不正确']);exit;
	}

	$data = file_get_contents('./Db/user.txt');

	$data = json_decode($data,true);


	foreach ($data as  $v) {
		if($v['email']==$email){
			echo json_encode(['error'=>1,'info'=>'邮箱已注册']);exit;
		}
	}

	echo json_encode(['error'=>0,'info'=>'邮箱可用']);exit;
}

function checkPhone()
{
	echo json_encode(['error'=>0,'info'=>'手机可用']);exit;
}

function checkUsername()
{
	echo json_encode(['error'=>0,'info'=>'用户名可用']);exit;
}

function checkPassword()
{
	echo json_encode(['error'=>0,'info'=>'密码可用']);exit;
}






 ?>