<?php 

header("Content-type: text/html; charset=utf-8");

function getSum($num)
{
	$sum = 0;

	for ($i=1; $i <=$num ; $i++) { 
		
		$sum+=$i;
	}
	return $sum;
}
/**
 * [Ftime 时间格式化处理]
 * @Author   Xuebingsi
 * @DateTime 2017-11-07T09:06:48+0800
 * @param    [type]                   $time [时间戳]
 */
function Ftime($time)
{
	$diff = time()-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return ceil($diff/60)."分钟之前";
	}elseif($diff<86400){
		return ceil($diff/3600)."小时之前";
	}elseif($diff<(86400*7)){
		return ceil($diff/86400)."天之前";
	}else{
		return date('Y-m-d H:i:s',$time);
	}
}

function conuntDay()
{
	$cn = 0;
	for ($i=0; $i < 365; $i++) { 

		$day = strtotime("-".$i." days");
		if(date('Y',$day)=='2016'){
			break;
		}
		$now = getdate($day);
		
		if($now['wday']==0){
			$cn++;
		}
	}
	return $cn;
}

















 ?>